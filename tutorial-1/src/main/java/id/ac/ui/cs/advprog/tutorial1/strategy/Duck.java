package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void setQuackBehavior(QuackBehavior behavior){
        quackBehavior = behavior;
    }

    public void setFlyBehavior(FlyBehavior behavior){
        flyBehavior = behavior;
    }

    public abstract void display();

    public abstract void swim();

}
