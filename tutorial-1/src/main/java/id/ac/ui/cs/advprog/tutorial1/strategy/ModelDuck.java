package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    @Override
    public void display() {
        System.out.println("Looks like model duck.");
    }

    @Override
    public void swim() {
        System.out.println("Just floats");
    }
}
