package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

    @Override
    public void display() {
        System.out.println("Looks like mallard.");
    }

    @Override
    public void swim() {
        System.out.println("Swims like a mallard.");
    }
}
